ini_setting { "test setting":
  ensure  => present,
  path    => '/tmp/test1.ini',
  section => 'gazonk',
  setting => 'foo',
  value   => 'bar',
}

ini_subsetting {'test subsetting':
  ensure            => present,
  section           => '',
  key_val_separator => '=',
  path              => '/tmp/test2.ini',
  setting           => 'flargle',
  subsetting        => '-foo',
  value             => 'arglebargle',
}

$defaults = { 'path' => '/tmp/test3.ini' }
$example = {
  'one' => {
    'here'  => 'there',
    'gone' => {
      'ensure' => 'absent'
    }
  },
  'two' => {
    'meep' => 'wibble',
  }
}
inifile::create_ini_settings($example, $defaults)
